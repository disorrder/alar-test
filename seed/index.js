import { MongoClient } from 'mongodb';
import faker from 'faker';

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export function generateUser() {
    const gender = getRandomInt(0, 1);
    const firstName = faker.name.firstName(gender);
    const lastName = faker.name.lastName(gender);
    const age = faker.datatype.number(100);
    const phone = faker.phone.phoneNumber('(###)-###-##-##');
    const country = faker.address.country();
    const city = faker.address.city();

    const username = [firstName, lastName].join('.').toLowerCase();
    const email = faker.internet.email(firstName, lastName).toLowerCase();
    const password = '123';

    return {
        email,
        username,
        password,

        firstName,
        lastName,
        gender,
        age,
        phone,
        address: {
            country,
            city,
        }
    };
}

export async function seed(count = 1e6) {
    const { DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME, AUTH_SECRET } = process.env;
    const DB = process.env.DB ?? `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`;

    const client = new MongoClient(DB, {
        useNewUrlParser: true,
    });

    try {
        console.info('Try to connect to', DB);
        await client.connect();
        console.info('MongoDB connected!');
    } catch (error) {
        throw new Error(error);
    }

    const usersCollection = client.db().collection('users');

    // Prepare data
    try {
        await usersCollection.drop();
    } catch(e) {}

    const newUsers = [];
    const insertions = []
    for (let it = 0; it < count; it++) {
        newUsers.push(generateUser());

        // Save memory
        if (newUsers.length >= 1000) {
            const insertion = usersCollection.insertMany(newUsers);
            insertions.push(insertion);
            newUsers.length = 0;
        }
    }

    await Promise.all(insertions);

    await client.close();
    console.info('Finished!');
    console.info(newUsers);
}

seed();
