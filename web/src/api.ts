import axios from "axios";
import { authStore } from "./pages/auth/store";

export const api = axios.create({
    baseURL: process.env.API
});

api.interceptors.request.use((config) => {
    const { access_token } = localStorage; // TODO: replace cookie
    if (access_token) {
        config.headers.Authorization = 'Bearer ' + access_token;
    }
    return config;
});

api.interceptors.response.use(null, (error) => {
    if (error.response.status === 401) {
        authStore.setAuthenticated(false);
    }
    return Promise.reject(error);
});
