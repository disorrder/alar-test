import { Icon, IconButton } from "@material-ui/core";
import React from "react";

interface Props {
    value: boolean;
    onChange: (value: boolean) => void;
    [key: string]: any;
}

export function FavoriteButton(props: Props) {
    const { value, onChange, ...rest } = props;

    const color = value ? "secondary" : "default";

    function handleClick() {
        onChange?.(!value);
    }

    return (
        <IconButton color={color} {...rest} onClick={handleClick}>
            <Icon>{value ? "star" : "star_outline"}</Icon>
        </IconButton>
    );
}
