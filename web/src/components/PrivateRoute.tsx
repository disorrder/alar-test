import React, { ReactElement } from "react";
import { Route, Redirect } from "react-router-dom";

interface Props {
    component: any;
    isAuthenticated: boolean;
    [key: string]: any;
}

export function PrivateRoute(props: Props): ReactElement {
    const { component: C, isAuthenticated, ...routeProps } = props;
    return (
        <Route
            {...routeProps}
            render={(props) => {
                return isAuthenticated === true ? (
                    <C {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: { from: props.location },
                        }}
                    />
                );
            }}
        />
    );
}
