import { Typography } from "@mui/material";
import React from "react";

export function Copyright(props: any) {
    return (
        <Typography
            variant="body2"
            color="text.secondary"
            align="center"
            {...props}
        >
            {"Copyright © Disorder, 2021. Special for Alar Studios."}
        </Typography>
    );
}
