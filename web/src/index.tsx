import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router } from "react-router-dom";
import App from "./App";

const history = createBrowserHistory();
const component = (
    <Router history={history}>
        <App />
    </Router>
);

ReactDOM.render(component, document.getElementById("root"));
