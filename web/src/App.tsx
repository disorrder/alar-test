import React, { useEffect, useState } from "react";
import { Redirect, Route, Switch, useHistory, withRouter } from "react-router";
import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { observer } from "mobx-react-lite";
import Users from "./pages/users";
import { PrivateRoute } from "./components/PrivateRoute";
import { authStore } from "./pages/auth/store";
import { usersStore } from "./pages/users/store";
import Login from "./pages/auth/login";
import Register from "./pages/auth/register";

const theme = createTheme();

function App(): React.ReactElement {
    // const history = useHistory(); //?
    const [_authStore] = useState(authStore); // Not so beautiful, I know

    useEffect(() => {
        usersStore.fetchMe().then((user) => {
            authStore.setUser(user);
        });
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <PrivateRoute
                    path="/users"
                    component={Users}
                    isAuthenticated={_authStore.isAuthenticated}
                />
                <Redirect exact from="/" to="/users" />
            </Switch>
        </ThemeProvider>
    );
}

export default withRouter(observer(App));
