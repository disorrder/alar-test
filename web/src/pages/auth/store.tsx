import { makeAutoObservable } from "mobx";
import { api } from "../../api";
import { IUser } from "../users/store";

export class AuthStore {
    #url = "/auth";

    isAuthenticated = !!localStorage.access_token;
    accessToken: string | null = localStorage.access_token ?? null;
    user: IUser | null = null;

    constructor() {
        makeAutoObservable(this);
    }

    setAuthenticated(v: boolean) {
        this.isAuthenticated = v;
        if (v === false) {
            this.setAccessToken(null);
        }
    }

    setAccessToken(v: string | null) {
        this.accessToken = v;
        if (v) {
            localStorage.access_token = v;
        } else {
            delete localStorage.access_token;
        }
    }

    setUser(v: IUser) {
        this.user = v;
    }

    async login(creds: ICredentials): Promise<void> {
        const { data } = await api.post<{ access_token: string }>(
            this.#url + "/login",
            creds
        );
        const { access_token } = data;
        this.setAuthenticated(true);
        this.setAccessToken(access_token);
    }

    async register(regData: Omit<IUser, '_id'>): Promise<void> {
        const { data } = await api.post<{ access_token: string }>(
            this.#url + "/register",
            regData
        );
        const { access_token } = data;
        this.setAuthenticated(true);
        this.setAccessToken(access_token);
    }
}

export const authStore = new AuthStore();

// Types

export interface ICredentials {
    username: string;
    password: string;
}
