// Clone from seed
function getRandomInt(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export async function generateUser() {
    const faker = await import('faker'); // split heavy lib
    const gender = getRandomInt(0, 1);
    const firstName = faker.name.firstName(gender);
    const lastName = faker.name.lastName(gender);
    const age = faker.datatype.number(100);
    const phone = faker.phone.phoneNumber("(###)-###-##-##");
    const country = faker.address.country();
    const city = faker.address.city();

    const username = [firstName, lastName].join(".").toLowerCase();
    const email = faker.internet.email(firstName, lastName).toLowerCase();
    const password = "123";

    return {
        email,
        username,
        password,

        firstName,
        lastName,
        gender,
        age,
        phone,
        address: {
            country,
            city,
        },
    };
}
