import React, { ChangeEvent, FormEvent, useState } from "react";
import {
    Alert,
    Avatar,
    Box,
    Button,
    Checkbox,
    Container,
    FormControlLabel,
    Grid,
    Icon,
    Link,
    TextField,
    Typography,
} from "@mui/material";
import LoadingButton from '@mui/lab/LoadingButton';
import { useHistory } from "react-router";
import { Copyright } from "../../components/Copyright";
import { authStore } from "./store";

export default function Login(): React.ReactElement {
    const history = useHistory();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    function handleInputUsername(event: ChangeEvent<HTMLInputElement>) {
        setUsername(event.target.value);
    }

    function handleInputPassword(event: ChangeEvent<HTMLInputElement>) {
        setPassword(event.target.value);
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        setLoading(true);
        setError("");
        try {
            await authStore.login({ username, password });
            authStore.setAuthenticated(true);
            // @ts-ignore
            const from = history.location.state?.from || "/users";
            history.push(from);
        } catch (error) {
            if (error.response.status === 401) {
                setError("Wrong credentials.");
            } else if (error.response) {
                const { status, statusText } = error.response;
                setError(`Unexpected error ${status}: ${statusText}`);
            } else {
                setError(error);
            }
        } finally {
            setLoading(false);
        }
    }

    return (
        <Container maxWidth="sm">
            <Box
                sx={{
                    marginTop: 8,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}
            >
                <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
                    <Icon>lock_outlined</Icon>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <Box
                    component="form"
                    onSubmit={handleSubmit}
                    noValidate
                    sx={{ mt: 1 }}
                >
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Email or Username"
                        name="username"
                        autoComplete="username"
                        autoFocus
                        onInput={handleInputUsername}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onInput={handleInputPassword}
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                        sx={{ mb: 1 }}
                    />

                    {!!error && (
                        <Alert severity="error">{error}</Alert>
                    )}

                    <LoadingButton
                        type="submit"
                        fullWidth
                        variant="contained"
                        loading={loading}
                        sx={{ my: 2 }}
                    >
                        Sign In
                    </LoadingButton>
                    <Grid container>
                        <Grid item xs>
                            <Link href="#" variant="body2">
                                Forgot password?
                                <br />
                                Sorry, you can't restore :c
                            </Link>
                        </Grid>
                        <Grid item>
                            <Link href="/register" variant="body2">
                                Don't have an account? Sign Up!
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
            <Copyright sx={{ mt: 8, mb: 4 }} />
        </Container>
    );
}
