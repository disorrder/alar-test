import { Divider } from "@material-ui/core";
import {
    Avatar,
    Box,
    Button,
    Checkbox,
    Container,
    FormControlLabel,
    Grid,
    Icon,
    Link,
    TextField,
    Typography,
} from "@mui/material";
import React, { FormEvent, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Copyright } from "../../components/Copyright";
import { Gender, IUser } from "../users/store";
import { generateUser } from "./generate";
import { authStore } from "./store";

export default function Register(): React.ReactElement {
    const history = useHistory();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [gender, setGender] = useState(Gender.MALE);
    const [age, setAge] = useState(0);
    const [country, setCountry] = useState("");
    const [city, setCity] = useState("");

    useEffect(() => {
        setUsername(
            [firstName, lastName]
                .filter((v) => v)
                .join(".")
                .toLowerCase()
        );
    }, [firstName, lastName]);

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        try {
            await authStore.register({
                firstName,
                lastName,
                username,
                email,
                password,
                gender,
                age,
                address: {
                    country,
                    city,
                },
            });
            authStore.setAuthenticated(true);
            // @ts-ignore
            const from = history.location.state?.from || "/users";
            history.push(from);
        } catch (error) {
            // TODO: messages
        }
    }

    async function handleClickRandom() {
        const formData = await generateUser();
        setFirstName(formData.firstName);
        setLastName(formData.lastName);
        setUsername(formData.username);
        setEmail(formData.email);
        setPassword(formData.password);
        setGender(formData.gender);
        setAge(formData.age);
        setCountry(formData.address.country);
        setCity(formData.address.city);
    }

    return (
        <Container maxWidth="sm">
            <Box
                sx={{
                    marginTop: 8,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                }}
            >
                <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
                    <Icon>lock_outlined</Icon>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>

                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="success"
                    sx={{ mt: 3, mb: 2 }}
                    onClick={handleClickRandom}
                >
                    Randomize form
                </Button>

                <Box
                    component="form"
                    onSubmit={handleSubmit}
                    noValidate
                    sx={{ mt: 1 }}
                >
                    <Grid container columnSpacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="First name"
                                name="firstName"
                                autoFocus
                                value={firstName}
                                onChange={(event) =>
                                    setFirstName(event.target.value)
                                }
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="Last name"
                                name="lastName"
                                value={lastName}
                                onChange={(event) =>
                                    setLastName(event.target.value)
                                }
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="Username"
                                name="username"
                                value={username}
                                onChange={(event) =>
                                    setUsername(event.target.value)
                                }
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="E-mail"
                                name="email"
                                value={email}
                                onChange={(event) =>
                                    setEmail(event.target.value)
                                }
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="Password"
                                value={password}
                                onChange={(event) =>
                                    setPassword(event.target.value)
                                }
                            />
                        </Grid>
                        <Grid item xs={12} mb={4}>
                            <TextField
                                margin="normal"
                                fullWidth
                                label="Life is too short to repeat passwords"
                                disabled
                            />
                        </Grid>

                        <Divider />

                        <Grid item xs={12} sm={6}>
                            <TextField
                                type="number"
                                margin="normal"
                                required
                                fullWidth
                                label="Age"
                                name="age"
                                value={age}
                                onChange={(event) =>
                                    setAge(+event.target.value)
                                }
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            {/* TODO: drop list */}
                            <TextField
                                select
                                margin="normal"
                                required
                                fullWidth
                                label="Gender"
                                name="gender"
                                value={gender}
                                onChange={(event) =>
                                    setGender(+event.target.value)
                                }
                            >
                                <option value={Gender.MALE}>Male</option>
                                <option value={Gender.FEMALE}>Female</option>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="Country"
                                name="address.country"
                                value={country}
                                onChange={(event) =>
                                    setCountry(event.target.value)
                                }
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="City"
                                name="address.city"
                                value={city}
                                onChange={(event) =>
                                    setCity(event.target.value)
                                }
                            />
                        </Grid>
                    </Grid>

                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Торжественно клянусь, что замышляю шалость и только шалость"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        Sign Up
                    </Button>

                    <Grid container>
                        <Grid item xs></Grid>
                        <Grid item>
                            <Link href="/login" variant="body2">
                                Already have an account? Sign In!
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
            <Copyright sx={{ mt: 8, mb: 4 }} />
        </Container>
    );
}
