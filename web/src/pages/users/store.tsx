import { makeAutoObservable, reaction } from "mobx";
import { api } from "../../api";

export class UsersStore {
    #url = "/users";

    isLoading = false;
    // _items: IUser[] = [];
    items: IUserTable[] = [];
    total: number = 0;

    // query affecting params
    search = ""; //wip
    sort = "";
    limit = 25;
    page = 0;

    constructor() {
        makeAutoObservable(this);
        reaction(
            () => this.query,
            () => this.update()
        );
    }

    get users() {
        return [...this.items];
    }

    get skip() {
        return this.page * this.limit;
    }

    get query() {
        const { search, sort, skip } = this;

        return {
            search,
            sort,
            skip,
        };
    }

    get maxPage() {
        const max = Math.floor(this.total / this.limit);
        return Math.max(max - 1, 0);
    }

    setSearch(v: string) {
        this.search = v;
    }

    setSort(v: string) {
        this.sort = v;
    }

    setPage(v: number) {
        this.page = Math.max(0, Math.min(v, this.maxPage));
    }

    setItems(items: IUser[]) {
        this.items = items.map((item, i) => {
            return {
                ...item,
                _key: this.query.skip + i + 1,
            };
        });
    }

    updateItem(data: Partial<IUser>) {
        const index = this.items.findIndex((v) => v._id === data._id);
        const item = this.items[index];
        this.items[index] = { ...item, ...data };
    }

    async update(): Promise<IUserTable[]> {
        this.isLoading = true;
        const { query } = this;
        const res = await api.get<IUser[]>(this.#url, { params: query });
        this.isLoading = false;

        this.total = +res.headers.total;
        this.setItems(res.data);
        return this.items;
    }

    async fetchMe(): Promise<IUser> {
        const { data } = await api.get<IUser>(this.#url + "/me");
        return data;
    }

    async addFavorite(id: string) {
        const item = this.items.find((v) => v._id === id);
        if (item.isFavorite === true) return;

        // Optimistic set
        this.updateItem({ _id: id, isFavorite: true });

        try {
            await api.put(`/user-favorites/${id}`);
        } catch (e) {
            this.updateItem({ _id: id, isFavorite: false });
        }
    }

    async removeFavorite(id: string) {
        const item = this.items.find((v) => v._id === id);
        if (item.isFavorite === false) return;

        // Optimistic set
        this.updateItem({ _id: id, isFavorite: false });

        try {
            await api.delete(`/user-favorites/${id}`);
        } catch (e) {
            this.updateItem({ _id: id, isFavorite: true });
        }
    }
}

export const usersStore = new UsersStore();

// Types

export interface IUser {
    _id: string;
    email: string;
    username: string;
    password: string;

    firstName?: string;
    lastName?: string;
    gender?: Gender;
    age?: number;
    phone?: string;
    address?: Partial<IUserAddress>;

    isFavorite?: boolean; //? mb move to IUserTable
}

export enum Gender {
    MALE,
    FEMALE,
}

export interface IUserAddress {
    country: string;
    city: string;
}

export interface IUserTable extends IUser {
    _key: number; // table row number
}
