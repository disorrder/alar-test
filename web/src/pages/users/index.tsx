import React, { ChangeEvent, KeyboardEvent, useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import {
    Box,
    Container,
    FormControl,
    Grid,
    Icon,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    Typography,
} from "@mui/material";
import { GridColDef, DataGrid, GridSortModel } from "@mui/x-data-grid";
import { usersStore } from "./store";
import { FavoriteButton } from "../../components/FavoriteButton";

const columns: GridColDef[] = [
    { field: "_key", headerName: "#", width: 50, sortable: false },
    {
        field: "isFavorite",
        headerName: "*",
        width: 50,
        sortable: false,
        renderCell: ({ value, row }) => {
            async function handleChange(newVal: boolean) {
                if (newVal) {
                    usersStore.addFavorite(row._id);
                } else {
                    usersStore.removeFavorite(row._id);
                }
            }

            return <FavoriteButton value={!!value} onChange={handleChange} />;
        },
    },
    { field: "username", headerName: "Username", width: 130 },
    { field: "firstName", headerName: "First name", width: 130 },
    { field: "lastName", headerName: "Last name", width: 130 },
    { field: "email", headerName: "E-mail", width: 150 },
    {
        field: "gender",
        headerName: "Gender",
        width: 120,
        valueFormatter: ({ value }) => {
            if (value == null) return "N/A";
            return value === 0 ? "Male" : "Female";
        },
    },
    { field: "age", headerName: "Age", width: 100 },
    { field: "phone", headerName: "Phone", width: 130 },
    {
        field: "address.country",
        headerName: "Country",
        width: 130,
        valueGetter: ({ row }) => row.address.country,
    },
    {
        field: "address.city",
        headerName: "City",
        width: 130,
        valueGetter: ({ row }) => row.address.city,
    },
];

function Users(): React.ReactElement {
    const [store] = useState(usersStore);
    const [search, setSearch] = useState(store.search);
    const { isLoading, users, total, limit } = store;

    useEffect(() => {
        store.update();
    }, []);

    function handleKeyDownSearch(event: KeyboardEvent<HTMLInputElement>) {
        if (event.code === "Enter") {
            handleClickSearch();
        }
    }

    function handleChangeSearch(event: ChangeEvent<HTMLInputElement>) {
        setSearch(event.target.value);
    }

    function handleClickSearch() {
        store.setSearch(search);
    }

    function handlePageChange(page: number) {
        store.setPage(page);
    }

    function handleSortModelChange(model: GridSortModel) {
        const sort = model
            .map(({ field, sort }) => {
                const sign = sort === "desc" ? "-" : "";
                return sign + field;
            })
            .join(" ");
        store.setSort(sort);
    }

    return (
        <Container sx={{ height: "calc(100vh - 80px)" }}>
            <Grid container mt={2} mb={1} justifyContent="end">
                <Grid
                    item
                    sx={{
                        display: "flex",
                        flexGrow: 1,
                        alignItems: "flex-end",
                    }}
                >
                    <Typography variant="h5">Table of Users</Typography>
                </Grid>

                <Grid item flexGrow={1}>
                    <FormControl variant="outlined" fullWidth size="small">
                        <InputLabel htmlFor="search-field">Search</InputLabel>
                        <OutlinedInput
                            id="search-field"
                            type="text"
                            value={search}
                            label="Search"
                            sx={{ px: 1 }}
                            onKeyDown={handleKeyDownSearch}
                            onChange={handleChangeSearch}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="search"
                                        edge="end"
                                        size="small"
                                        onClick={handleClickSearch}
                                    >
                                        <Icon>search</Icon>
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </Grid>
            </Grid>

            <DataGrid
                rows={users}
                columns={columns}
                pageSize={limit}
                rowCount={total}
                filterMode="server"
                sortingMode="server"
                paginationMode="server"
                loading={isLoading}
                getRowId={(v) => v._key}
                disableColumnMenu={true}
                onSortModelChange={handleSortModelChange}
                onPageChange={handlePageChange}
            />
        </Container>
    );
}

export default observer(Users);
