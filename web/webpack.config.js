const path = require("path");
const { EnvironmentPlugin } = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const dotenv = require("dotenv");

module.exports = (env, { mode = "development" }) => {
    console.info(env, mode);

    const { DOCKER } = process.env;
    if (!DOCKER) {
        dotenv.config({ path: path.resolve(__dirname, "../.env") });
    }
    const { API_HOST = '0.0.0.0', API_PORT } = process.env;

    return {
        mode,
        context: path.resolve(__dirname, "src"),
        entry: "./index.tsx",
        output: {
            filename: "[name].js",
            path: path.resolve(__dirname, "dist"),
            publicPath: "/",
            clean: true,
        },
        devServer: {
            static: {
                directory: path.resolve(__dirname, "public"),
            },
            compress: true,
            port: 8080,
            open: true,
            historyApiFallback: true,
            proxy: {
                "/api": {
                    target: `http://${API_HOST}:${API_PORT}`,
                    pathRewrite: { "^/api/": "/" },
                },
            },
        },
        devtool: "inline-source-map",
        resolve: {
            extensions: [".tsx", ".ts", ".js"],
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: "ts-loader",
                    exclude: /node_modules/,
                },
                {
                    // pictures and fonts
                    test: /\.(jpeg|jpg|png|gif|woff2?|svg|ttf|eot|fnt)$/i,
                    loader: "file-loader",
                    options: {
                        name: "[path][name].[ext]",
                        esModule: false,
                    },
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: "index.html",
            }),
            new EnvironmentPlugin(["API"]),
        ],
    };
};
