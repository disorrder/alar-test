import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Gender, IUserAddress } from './user.dto';

@Schema({
    // versionKey: false,
})
export class User {
    @Prop({ required: true, unique: true }) email: string;
    @Prop({ required: true, unique: true }) username: string;
    @Prop({ required: true, select: false }) password: string;

    @Prop() firstName: string;
    @Prop() lastName: string;
    @Prop() gender: Gender;
    @Prop() age: number;
    @Prop() phone: string;
    @Prop(
        raw({
            country: { type: String },
            city: { type: String },
        })
    )
    address: IUserAddress;
}

export type UserDocument = User & Document;
export const UserSchema = SchemaFactory.createForClass(User);

// TODO: createIndex text
// UserSchema.index({
//     email: 'text',
//     username: 'text',
//     firstName: 'text',
//     lastName: 'text',
//     gender: 'text',
//     age: 'text',
//     phone: 'text',
//     'address.country': 'text',
//     'address.city': 'text',
// });
