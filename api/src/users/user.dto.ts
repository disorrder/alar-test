export interface CreateUserDTO {
    email: string;
    username: string;
    password: string;

    firstName?: string;
    lastName?: string;
    gender?: Gender;
    age?: number;
    phone?: string;
    address?: IUserAddress;
}

export type UpdateUserDTO = Omit<
    CreateUserDTO,
    'email' | 'username' | 'password'
>;

export enum Gender {
    MALE,
    FEMALE,
}

export interface IUserAddress {
    country: string;
    city: string;
}
