import {
    Body,
    Controller,
    Get,
    Patch,
    Post,
    Query,
    Req,
    Res,
    UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request, Response } from 'express';
import { CreateUserDTO } from './user.dto';
import { User, UserDocument } from './user.schema';
import { IFindQuery, UsersService } from './users.service';

@Controller('users')
@UseGuards(AuthGuard('jwt'))
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    //? Not sure this method is usable
    @Post()
    async create(@Body() body: CreateUserDTO): Promise<User> {
        return this.usersService.create(body);
    }

    @Get()
    async find(
        @Query() query: Partial<IFindQuery>,
        @Req() req: Request,
        @Res() res: Response
    ): Promise<UserDocument[]> {
        const { _id: userId } = req.user as UserDocument;
        const { data, total } = await this.usersService.find(query, userId);
        res.setHeader('Total', total); //? TODO: or think how to set headers without res
        res.json(data); //? TODO: think how to send result automatically
        return data;
    }

    @Get('/me')
    async me(@Req() req: Request): Promise<UserDocument> {
        const { _id } = req.user as UserDocument;
        const user = await this.usersService.findById(_id);
        return user;
    }
}
