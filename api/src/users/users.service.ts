import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateUserDTO } from './user.dto';
import { User, UserDocument } from './user.schema';
import {
    UserFavorites,
    UserFavoritesDocument,
    UserFavoritesSchema,
} from '../user-favorites/user-favorites.schema';
import { UserFavoritesService } from 'src/user-favorites/user-favorites.service';

export interface IFindQuery {
    search: string;
    sort: string;
    skip: number;
    limit: number;
}

interface IFindResult {
    data: UserDocument[];
    total: number;
}

export interface IUpdateQuery {
    [key: string]: boolean;
}

@Injectable()
export class UsersService {
    limit = 25;
    constructor(
        @InjectModel(User.name) private model: Model<UserDocument>,
        private userFavoritesService: UserFavoritesService
    ) {}

    //? not used
    // async getCount(): Promise<number> {
    //     return this.model.estimatedDocumentCount();
    // }

    async create(data: CreateUserDTO): Promise<UserDocument> {
        const newUser = new this.model(data);
        return newUser.save();
    }

    async findById(id: string): Promise<UserDocument> {
        return this.model.findById(id);
    }

    /**
     * @param login means username or email
     */
    async findByLogin(login: string): Promise<UserDocument> {
        const field = ~login.indexOf('@') ? 'email' : 'username';
        return this.model.findOne({ [field]: login }).select('+password');
    }

    async find(
        query: Partial<IFindQuery>,
        userId: Types.ObjectId
    ): Promise<IFindResult> {
        const { search, sort, skip = 0 } = query;
        const limit = query.limit ?? this.limit;

        const searchReq = this.model.aggregate(null, {
            allowDiskUse: true,
        });
        // if (search) searchReq.match({ $text: { $search: search } }); // Doesn't work :c
        if (search) {
            const query = { $regex: search };
            searchReq.match({
                $or: [
                    { username: query },
                    { email: query },
                    { firstName: query },
                    { lastName: query },
                    { gender: query },
                    { age: query },
                    { phone: query },
                    { 'address.country': query },
                    { 'address.city': query },
                ],
            });
        }

        if (sort) searchReq.sort(sort);
        searchReq.facet({
            data: [{ $skip: +skip }, { $limit: +limit }],
            meta: [{ $count: 'total' }],
        });

        const favorites = await this.userFavoritesService.findByUserId(userId);
        const favoritesSet = new Set(favorites?.map((v) => v?.toString()));

        const [{ data, meta }] = await searchReq.exec();
        const total = meta[0]?.total ?? 0;

        const mappedData = data.map((user: UserDocument) => {
            return {
                ...user,
                isFavorite: favoritesSet.has(user._id.toString()),
            };
        });

        return { data: mappedData, total };
    }
}
