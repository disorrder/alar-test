import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { CreateUserDTO } from 'src/users/user.dto';
import { UserDocument } from 'src/users/user.schema';
import { AuthService } from './auth.service';

// TODO: override Request interface
// namespace express {
//     interface Request {
//         user: UserDocument;
//     }
// }

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('login')
    @UseGuards(AuthGuard('local'))
    async login(@Req() req: Request) {
        return this.authService.login(req.user as UserDocument);
    }

    @Post('register')
    async register(@Body() body: CreateUserDTO) {
        console.log('Reg user:', body);
        return this.authService.register(body);
    }
}
