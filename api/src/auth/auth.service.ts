import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDTO } from 'src/users/user.dto';
import { UserDocument } from 'src/users/user.schema';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService
    ) {}

    async validateUser(username: string, password: string): Promise<any> {
        const user = await this.usersService.findByLogin(username);
        if (user?.password === password) {
            const { password, ...userData } = user.toObject();
            return userData;
        }
        return null;
    }

    async login(user: UserDocument) {
        const payload = { username: user.username, sub: user._id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async register(userData: CreateUserDTO) {
        const user = await this.usersService.create(userData);
        return this.login(user);
    }
}
