import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { UserFavoritesModule } from './user-favorites/user-favorites.module';

const { DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME } = process.env;
const DB =
    process.env.DB ??
    `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?authSource=admin`;
console.info('Try to connect', DB);

@Module({
    imports: [
        MongooseModule.forRoot(DB),
        UsersModule,
        AuthModule,
        UserFavoritesModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
