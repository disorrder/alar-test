import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema, Types, Document } from 'mongoose';

@Schema({
    versionKey: false,
})
export class UserFavorites {
    @Prop({
        required: true,
        unique: true,
        type: MongooseSchema.Types.ObjectId,
        ref: 'User',
    })
    userId: Types.ObjectId;

    @Prop({
        type: [MongooseSchema.Types.ObjectId],
        ref: 'User',
    })
    favorites: Types.ObjectId[];
}

export type UserFavoritesDocument = UserFavorites & Document;
export const UserFavoritesSchema = SchemaFactory.createForClass(UserFavorites);
