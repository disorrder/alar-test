import { Controller, Delete, Param, Put, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { ObjectId } from 'mongoose';
import { UserDocument } from 'src/users/user.schema';
import { UserFavoritesService } from './user-favorites.service';

@Controller('user-favorites')
@UseGuards(AuthGuard('jwt'))
export class UserFavoritesController {
    constructor(private readonly userFavoriteService: UserFavoritesService) {}

    @Put(':userId')
    async add(@Param('userId') userId: string, @Req() req: Request) {
        const { _id: ownerId } = req.user as UserDocument;
        return this.userFavoriteService.add(ownerId, userId);
    }

    @Delete(':userId')
    async remove(@Param('userId') userId: string, @Req() req: Request) {
        const { _id: ownerId } = req.user as UserDocument;
        return this.userFavoriteService.remove(ownerId, userId);
    }
}
