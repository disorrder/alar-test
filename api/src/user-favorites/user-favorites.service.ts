import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { UserFavorites, UserFavoritesDocument } from './user-favorites.schema';

@Injectable()
export class UserFavoritesService {
    constructor(
        @InjectModel(UserFavorites.name)
        private model: Model<UserFavoritesDocument>
    ) {}

    async findByUserId(userId: Types.ObjectId) {
        const res = await this.model.findOne({ userId });
        if (!res) return [];

        const favorites = res.favorites;
        return favorites;
    }

    async add(_userId: string, _favId: string) {
        const userId = new Types.ObjectId(_userId);
        const favId = new Types.ObjectId(_favId);

        const doc = await this.model.findOne({ userId });

        if (doc) {
            await doc.update(
                { $addToSet: { favorites: favId } },
                { new: true }
            );
            return 'updated';
        } else {
            const newDoc = new this.model({
                userId,
                favorites: [favId],
            });
            await newDoc.save();
            return 'created';
        }
    }

    async remove(_userId: string, _favId: string) {
        const userId = new Types.ObjectId(_userId);
        const favId = new Types.ObjectId(_favId);
        const doc = await this.model.findOneAndUpdate(
            { userId },
            { $pull: { favorites: favId } },
            { new: true }
        );
        return 'updated';
    }
}
