import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserFavoritesController } from './user-favorites.controller';
import { UserFavorites, UserFavoritesSchema } from './user-favorites.schema';
import { UserFavoritesService } from './user-favorites.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: UserFavorites.name, schema: UserFavoritesSchema },
        ]),
    ],
    exports: [UserFavoritesService],
    controllers: [UserFavoritesController],
    providers: [UserFavoritesService],
})
export class UserFavoritesModule {}
