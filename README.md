# Тестовое задание для позиции Full Stack Developer (JavaScript):

## Description
Приложение "База пользователей":

Приложение должно давать возможность зарегистрировать/залогинить пользователя.
Должна быть возможность просмотреть всех пользователей зарегистрировавшихся в приложении.
Пользоваться просмотром пользователей можно только залогинившись.
Приложение должно дать возможность искать, фильтровать и сортировать других пользователей по их: имени, фамилии, возрасту, полу, телефону, городу, стране, имени пользователя и электронной почте.
Пользователь может сохранить любого другого пользователя в список фаворитов, для быстрого получения контакта.

Требования:
Seed базы >=1M пользователей с фэйковыми данными - для поиска, фильтра и сортировки.
Проект должен запускаться в Docker или Kubernetes.
SOA (Service Oriented Architecture) [бонус].
По возможности комментарии к коду [бонус].
Обязательно показать использование как ORM/ODM так и Raw Query.
Описание запуска проекта в виде: README.md
Проверьте ваш проект на работоспособность - это очевидно.

Успехов! На задание дается 72 часа.

## Comments
Задание довольно объёмное, в 3 дня уложиться не получилось. Пришлось потратить ещё пару вечеров после основной работы на доработку.
По результатам собеседования мне выслали оффер, от которого я в итоге отказался.


## Demo
[https://alar-test.disordered.ru/](https://alar-test.disordered.ru/)

## Install
Just install Docker and Docker Compose

## Run Dev Environment
```bash
docker-compose up -d
```
It runs MongoDB, Node API and Webpack dev server.
Open browser at `http://localhost:8000` for quick view

## Stop Dev Environment
```bash
docker-compose down
```

## Seed Database
> NOTE: Run API before seed!
If you have Nodejs installed:
```bash
npm start -w seed
```
OR
```bash
cd seed
docker build . -t seed
docker run --rm --env-file ../.env --network alar-test_default seed
```

You can login from any user with password `123`. Username is lowercase of `${firstName}.${lastName}`.
You can get names from terminal after executing seed command.

## Start Web for development
```bash
cd web && npm start
```
## Authorization
Session will expire in 30 minutes, just keep in mind if you will be log out unexpected. It was made for purpose.

There is also no form validations yet.
